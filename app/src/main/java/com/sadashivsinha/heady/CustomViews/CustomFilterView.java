package com.sadashivsinha.heady.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sadashivsinha.heady.Interfaces.OnOptionSelectedListener;
import com.sadashivsinha.heady.R;

/**
 * Purpose – {To generate a custom filter view}
 *
 * @author Sadashiv Sinha
 * Created on 2020-01-17
 */
public class CustomFilterView extends RelativeLayout {

    public CustomFilterView(final OnOptionSelectedListener listener, final Context context, String filterOption, int index) {
        super(context);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = mInflater.inflate(R.layout.custom_filter_view, this, true);

        TextView tvFilterOption = mView.findViewById(R.id.tv_filter_option);
        tvFilterOption.setText(filterOption);

        mView.setOnClickListener(view -> listener.onSelected(index));
    }
}
