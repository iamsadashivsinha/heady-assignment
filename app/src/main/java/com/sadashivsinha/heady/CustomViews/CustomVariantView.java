package com.sadashivsinha.heady.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sadashivsinha.heady.R;

/**
 * Purpose – {To generate a custom variant view}
 *
 * @author Sadashiv Sinha
 * Created on 2020-01-17
 */
public class CustomVariantView extends RelativeLayout {

    public CustomVariantView(final Context context, int color, int size, int price) {
        super(context);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = mInflater.inflate(R.layout.custom_variant_view, this, true);
        
        ImageView imageViewColor = mView.findViewById(R.id.iv_color);
        imageViewColor.setImageResource(color);
        
        TextView tvVariantSize = mView.findViewById(R.id.tv_variant_size);
        tvVariantSize.setText(String.valueOf(size));
        
        TextView tvVariantPrice = mView.findViewById(R.id.tv_price);
        tvVariantPrice.setText(String.valueOf(price));

        mView.setClickable(false);

    }
}
