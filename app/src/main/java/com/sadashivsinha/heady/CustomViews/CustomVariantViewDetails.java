package com.sadashivsinha.heady.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.sadashivsinha.heady.R;

/**
 * Purpose – {To generate a custom variant details view}
 *
 * @author Sadashiv Sinha
 * Created on 2020-01-17
 */
public class CustomVariantViewDetails extends RelativeLayout {

    TextView tvProductPrice, tvProductColor, tvProductSize;
    CardView cardViewMain;

    public CustomVariantViewDetails(final Context context, String color, int size, int price) {
        super(context);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = mInflater.inflate(R.layout.custom_variant_view_details, this, true);

        cardViewMain = mView.findViewById(R.id.card_view);

        tvProductPrice = mView.findViewById(R.id.tv_product_price);
        tvProductColor = mView.findViewById(R.id.tv_product_color);
        tvProductSize = mView.findViewById(R.id.tv_product_size);

        tvProductPrice.setText("Rs " + price + " /-");
        tvProductColor.setText("Colour : " + color);
        tvProductSize.setText("Size : " + size);
    }

    public void changeSelectedStatus(boolean isSelected) {
        if(isSelected) {
            cardViewMain.setCardBackgroundColor(getResources().getColor(R.color.colorPurple));
            tvProductPrice.setTextColor(getResources().getColor(R.color.colorWhite));
            tvProductColor.setTextColor(getResources().getColor(R.color.colorAccent));
            tvProductSize.setTextColor(getResources().getColor(R.color.colorAccent));
        }
        else {
            cardViewMain.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvProductPrice.setTextColor(getResources().getColor(R.color.colorBlack));
            tvProductColor.setTextColor(getResources().getColor(R.color.colorTextLight));
            tvProductSize.setTextColor(getResources().getColor(R.color.colorTextLight));
        }
    }
}
