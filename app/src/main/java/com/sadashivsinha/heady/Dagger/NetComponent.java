package com.sadashivsinha.heady.Dagger;

import android.app.Application;

import com.sadashivsinha.heady.Networking.NetworkModule;
import com.sadashivsinha.heady.Utilities.ActivityModule;
import com.sadashivsinha.heady.Utilities.MyApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

/**
 * Created by sadashivsinha on 16/01/20
 */

@Component(modules = {ActivityModule.class, AndroidInjectionModule.class, NetworkModule.class})
@Singleton
public interface NetComponent extends AndroidInjector<MyApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        NetComponent build();
    }

    void inject(MyApp app);
}