package com.sadashivsinha.heady.Utilities;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.sadashivsinha.heady.Model.DataModel;
import com.sadashivsinha.heady.Model.DataModelDao;

@Database(entities = {DataModel.class}, version = 1, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class MyDatabase extends RoomDatabase {

    // --- SINGLETON ---
    private static volatile MyDatabase INSTANCE;

    // --- DAO ---
    public abstract DataModelDao userDao();
}