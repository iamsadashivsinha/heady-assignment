package com.sadashivsinha.heady.Utilities;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.sadashivsinha.heady.Model.DataModel;

import javax.inject.Inject;

public class DataViewModel extends ViewModel {

    private LiveData<DataModel> data;
    private DataRepository dataRepository;

    @Inject
    public DataViewModel(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    // ----

    public void init() {
        if (this.data != null) {
            return;
        }
        data = dataRepository.getData();
    }

    public LiveData<DataModel> getData() {
        return this.data;
    }
}