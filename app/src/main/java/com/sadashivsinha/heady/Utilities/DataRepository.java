package com.sadashivsinha.heady.Utilities;


import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;

import com.sadashivsinha.heady.Model.DataModel;
import com.sadashivsinha.heady.Model.DataModelDao;
import com.sadashivsinha.heady.Networking.NetworkService;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class DataRepository {

    private static int FRESH_TIMEOUT_IN_MINUTES = 1;

    private final NetworkService networkService;
    private final DataModelDao dataModelDao;
    private final Executor executor;

    @Inject
    public DataRepository(NetworkService networkService, DataModelDao dataModelDao, Executor executor) {
        this.networkService = networkService;
        this.dataModelDao = dataModelDao;
        this.executor = executor;
    }

    // ---

    public LiveData<DataModel> getData() {
        refreshData(); // try to refresh data if possible from Api
        return dataModelDao.loadData(); // return a LiveData directly from the database.
    }

    // ---

    private void refreshData() {
        executor.execute(() -> {
            // Check if data was fetched recently
            boolean freshDataExist = (dataModelDao.loadFreshData(getMaxRefreshTime(new Date())) != null);
            // If data have to be updated
            if (!freshDataExist) {
                networkService.getAllData().enqueue(new Callback<DataModel>() {
                    @Override
                    public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                        Log.d("Data_Refresh : ", "Data refreshed from network at " + new Date().toString());
                        Log.d("Data_Response : ", response.toString());
                        executor.execute(() -> {
                            DataModel dataModel = response.body();
                            dataModel.setLastRefresh(new Date());
                            dataModelDao.saveData(dataModel);
                        });
                    }

                    @Override
                    public void onFailure(Call<DataModel> call, Throwable t) { }
                });
            }
        });
    }

    // ---

    private Date getMaxRefreshTime(Date currentDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MINUTE, -FRESH_TIMEOUT_IN_MINUTES);
        return cal.getTime();
    }
}