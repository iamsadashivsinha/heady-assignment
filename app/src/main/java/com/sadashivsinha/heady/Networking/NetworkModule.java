package com.sadashivsinha.heady.Networking;

import android.app.Application;

import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sadashivsinha.heady.Model.DataModelDao;
import com.sadashivsinha.heady.Utilities.DataRepository;
import com.sadashivsinha.heady.Utilities.MyDatabase;
import com.sadashivsinha.heady.Utilities.ViewModelModule;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sadashivsinha on 16/01/20.
 */


@Module(includes = ViewModelModule.class)
public class NetworkModule {

    // --- DATABASE INJECTION ---

    @Provides
    @Singleton
    MyDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application,
                MyDatabase.class, "MyDatabase.db")
                .build();
    }

    @Provides
    @Singleton
    DataModelDao provideDataDao(MyDatabase database) {
        return database.userDao();
    }

    // --- REPOSITORY INJECTION ---

    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }

    @Provides
    @Singleton
    DataRepository provideUserRepository(NetworkService networkService, DataModelDao dataDao, Executor executor) {
        return new DataRepository(networkService, dataDao, executor);
    }

    // --- NETWORK INJECTION ---

    private static String BASE_URL = "https://stark-spire-93433.herokuapp.com/";

    @Provides
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    Retrofit provideRetrofit(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    NetworkService provideApiWebservice(Retrofit restAdapter) {
        return restAdapter.create(NetworkService.class);
    }
}