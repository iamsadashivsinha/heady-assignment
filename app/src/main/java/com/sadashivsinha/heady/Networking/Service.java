package com.sadashivsinha.heady.Networking;

import android.util.Log;

import com.google.gson.Gson;
import com.sadashivsinha.heady.Model.DataModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sadashivsinha on 16/01/20.
 */

public class Service {

    private final NetworkService networkService;
    private String TAG = Service.class.getSimpleName();

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public void getAllData(final AllDataCallback callback) {

        Call<DataModel> call = networkService.getAllData();

        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                Log.d("DATA : ", String.valueOf(response));

                try {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                        return;
                    }

                    if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                        String errorResponse = response.errorBody().string();
                        Log.d(TAG, "onResponse: response : " + errorResponse);
                        Gson gson = new Gson();
                        try {
//                            Success success = gson.fromJson("{\"message\":"+ errorResponse.trim(),
//                                    Success.class);
                            Success success = gson.fromJson(errorResponse.trim(),
                                    Success.class);


                            callback.onError(success.getMessage());
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: Exception : " + e.getMessage());
                            callback.onError(new NetworkError(e));
                        }

                        return;
                    }
                } catch (Exception e) {
                    callback.onError(new NetworkError(e));
                    Log.e(TAG, "onResponse: exception :: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                Log.e(TAG, "onFailure: Exception : " + t.getMessage());
                Log.e(TAG, "onFailure: Call : " + call.toString());
                callback.onError(new NetworkError(t));
            }
        });
    }


    public interface AllDataCallback {
        void onSuccess(DataModel allData);

        void onError(NetworkError networkError);

        void onError(String errorMsg);
    }

}
