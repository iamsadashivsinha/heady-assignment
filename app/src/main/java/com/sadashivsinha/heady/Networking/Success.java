package com.sadashivsinha.heady.Networking;

/**
 * Created by sadashivsinha on 16/01/20.
 */

public class Success {

    public int getCode() {
        return code;
    }

    public int getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    private int code;
    private int success;
    private String message;
}
