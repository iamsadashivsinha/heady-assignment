package com.sadashivsinha.heady.Networking;

import com.sadashivsinha.heady.Model.DataModel;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by sadashivsinha on 16/01/20.
 */

public interface NetworkService {

    @GET(AppConfig.DATA)
    Call<DataModel> getAllData();
}
