package com.sadashivsinha.heady.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.sadashivsinha.heady.R;

/**
 * Created by sadashivsinha on 17/01/20
 */
public class BoldFontTextView extends androidx.appcompat.widget.AppCompatTextView {

    public BoldFontTextView(Context context) {
        super(context);
        init();

    }

    public BoldFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoldFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    public void setTypeface(Typeface tf, int style) {

        super.setTypeface(tf, style);

    }


    public void init() {
        Typeface typeface;

        if (Build.VERSION.SDK_INT >= 26) {
            typeface = getResources().getFont(R.font.font_bold);

        }
        else
        {
            typeface = ResourcesCompat.getFont(getContext(), R.font.font_bold);
        }

        setTypeface(typeface);
    }
}
