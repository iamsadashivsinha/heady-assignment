package com.sadashivsinha.heady.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.sadashivsinha.heady.R;

/**
 * Created by sadashivsinha on 17/01/20
 */
public class RegularFontTextView extends androidx.appcompat.widget.AppCompatTextView {

    public RegularFontTextView(Context context) {
        super(context);
        init();

    }

    public RegularFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RegularFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    public void setTypeface(Typeface tf, int style) {

        super.setTypeface(tf, style);

    }


    public void init() {
        Typeface typeface;

        if (Build.VERSION.SDK_INT >= 26) {
            typeface = getResources().getFont(R.font.font_regular);

        }
        else
        {
            typeface = ResourcesCompat.getFont(getContext(), R.font.font_regular);
        }

        setTypeface(typeface);
    }
}
