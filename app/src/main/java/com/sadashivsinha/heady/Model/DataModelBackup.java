package com.sadashivsinha.heady.Model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sadashivsinha on 16/01/20.
 */

public class DataModelBackup {

    @Nullable
    @SerializedName("categories")
    private List<Categories> categories;

    @Nullable
    @SerializedName("rankings")
    private List<Rankings> rankings;


    private class Categories {
        @Nullable
        @SerializedName("id")
        private int id;

        @Nullable
        @SerializedName("name")
        private String name;

        @Nullable
        @SerializedName("products")
        private List<Products> products;

        private class Products {
            @Nullable
            @SerializedName("id")
            private int id;

            @Nullable
            @SerializedName("name")
            private String name;

            @Nullable
            @SerializedName("date_added")
            private String dateAdded;

            @Nullable
            @SerializedName("variants")
            private List<Variants> variants;

            @Nullable
            @SerializedName("tax")
            private Tax tax;

            private class Variants {
                @Nullable
                @SerializedName("id")
                private int id;

                @Nullable
                @SerializedName("color")
                private String color;

                @Nullable
                @SerializedName("size")
                private int size;

                @Nullable
                @SerializedName("price")
                private int price;
            }

            private class Tax {
                @Nullable
                @SerializedName("name")
                private String name;

                @Nullable
                @SerializedName("value")
                private float value;

                private String getName() {
                    return name;
                }

                private float getValue() {
                    return value;
                }
            }

            private int getId() {
                return id;
            }

            private String getName() {
                return name;
            }

            private String getDateAdded() {
                return dateAdded;
            }

            private List<Variants> getVariants() {
                return variants;
            }

            private Tax getTax() {
                return tax;
            }
        }

        private int getId() {
            return id;
        }

        private String getName() {
            return name;
        }

        private List<Products> getProducts() {
            return products;
        }
    }

    private class Rankings {
        @Nullable
        @SerializedName("ranking")
        private String rankingByTitle;

        @Nullable
        @SerializedName("date_added")
        private String dateAdded;

        @Nullable
        @SerializedName("products")
        private List<Products> products;

        private class Products {
            @Nullable
            @SerializedName("id")
            private int id;

            @Nullable
            @SerializedName("view_count")
            private String viewCount;

            private int getId() {
                return id;
            }

            private String getViewCount() {
                return viewCount;
            }
        }

        private String getRankingByTitle() {
            return rankingByTitle;
        }

        private String getDateAdded() {
            return dateAdded;
        }

        private List<Products> getProducts() {
            return products;
        }
    }

    private List<Categories> getCategories() {
        return categories;
    }

    private List<Rankings> getRankings() {
        return rankings;
    }
}
