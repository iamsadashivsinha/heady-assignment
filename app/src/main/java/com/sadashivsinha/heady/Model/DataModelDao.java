package com.sadashivsinha.heady.Model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.Date;

import static androidx.room.OnConflictStrategy.REPLACE;

/**
 * Created by sadashivsinha on 16/01/20.
 */


@Dao
public interface DataModelDao {

    @Insert(onConflict = REPLACE)
    void saveData(DataModel dataModel);

    @Query("SELECT * FROM dataModel")
    LiveData<DataModel> loadData();

    @Query("SELECT * FROM dataModel WHERE lastRefresh > :lastRefreshMax LIMIT 1")
    DataModel loadFreshData(Date lastRefreshMax);
}
