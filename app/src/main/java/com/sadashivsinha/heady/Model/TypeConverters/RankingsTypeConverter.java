package com.sadashivsinha.heady.Model.TypeConverters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.sadashivsinha.heady.Model.DataModel;

public class RankingsTypeConverter {
    @TypeConverter
    public static DataModel.Rankings[] fromString(String rankingJson) {
        if (rankingJson == null) return null;

        Gson gson = new Gson();
        DataModel.Rankings[] rankings = gson.fromJson(rankingJson, DataModel.Rankings[].class);
        return rankings;
    }

    @TypeConverter
    public static String toString(DataModel.Rankings[] rankingJson) {
        if (rankingJson == null) return null;

        Gson gson = new Gson();
        String rankingJsonFull = gson.toJson(rankingJson);
        return rankingJsonFull;
    }
}
