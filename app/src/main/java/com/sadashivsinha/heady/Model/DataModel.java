package com.sadashivsinha.heady.Model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sadashivsinha.heady.Model.TypeConverters.CategoriesTypeConverter;
import com.sadashivsinha.heady.Model.TypeConverters.RankingsTypeConverter;

import java.util.Date;
import java.util.List;

/**
 * Created by sadashivsinha on 16/01/20.
 */

@Entity
public class DataModel {

    @PrimaryKey
    public int id;

    @Nullable
    @SerializedName("categories")
    @Expose
    @TypeConverters(CategoriesTypeConverter.class)
    public Categories[] categories;

    @Nullable
    @SerializedName("rankings")
    @Expose
    @TypeConverters(RankingsTypeConverter.class)
    public Rankings[] rankings;

    Date lastRefresh;

    public Date getLastRefresh() {
        return lastRefresh;
    }

    public void setLastRefresh(Date lastRefresh) {
        this.lastRefresh = lastRefresh;
    }

    public class Categories {
        @PrimaryKey
        @NonNull
        @SerializedName("id")
        @Expose
        public int id;

        @Nullable
        @SerializedName("name")
        @Expose
        public String name;

        @Nullable
        @SerializedName("products")
        @Expose
        public List<Products> products;

        public class Products {
            @PrimaryKey
            @NonNull
            @SerializedName("id")
            @Expose
            public int id;

            @Nullable
            @SerializedName("name")
            @Expose
            public String name;

            @Nullable
            @SerializedName("date_added")
            @Expose
            public String dateAdded;

            @Nullable
            @SerializedName("variants")
            @Expose
            public List<Variants> variants;

            @Nullable
            @SerializedName("tax")
            @Expose
            public Tax tax;

            public class Variants {
                @PrimaryKey
                @NonNull
                @SerializedName("id")
                @Expose
                public int id;

                @Nullable
                @SerializedName("color")
                @Expose
                public String color;

                @Nullable
                @SerializedName("size")
                @Expose
                public int size;

                @Nullable
                @SerializedName("price")
                @Expose
                public int price;

                public Variants(int id, @Nullable String color, int size, int price) {
                    this.id = id;
                    this.color = color;
                    this.size = size;
                    this.price = price;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                @Nullable
                public String getColor() {
                    return color;
                }

                public void setColor(@Nullable String color) {
                    this.color = color;
                }

                public int getSize() {
                    return size;
                }

                public void setSize(int size) {
                    this.size = size;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }
            }

            public class Tax {
                @Nullable
                @SerializedName("name")
                @Expose
                public String name;

                @Nullable
                @SerializedName("value")
                @Expose
                public float value;

                public Tax(@Nullable String name, float value) {
                    this.name = name;
                    this.value = value;
                }

                public String getName() {
                    return name;
                }

                public float getValue() {
                    return value;
                }

                public void setName(@Nullable String name) {
                    this.name = name;
                }

                public void setValue(float value) {
                    this.value = value;
                }
            }

            public int getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getDateAdded() {
                return dateAdded;
            }

            public List<Variants> getVariants() {
                return variants;
            }

            public Tax getTax() {
                return tax;
            }

            public Products(int id, @Nullable String name, @Nullable String dateAdded, @Nullable List<Variants> variants, @Nullable Tax tax) {
                this.id = id;
                this.name = name;
                this.dateAdded = dateAdded;
                this.variants = variants;
                this.tax = tax;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setName(@Nullable String name) {
                this.name = name;
            }

            public void setDateAdded(@Nullable String dateAdded) {
                this.dateAdded = dateAdded;
            }

            public void setVariants(@Nullable List<Variants> variants) {
                this.variants = variants;
            }

            public void setTax(@Nullable Tax tax) {
                this.tax = tax;
            }
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public List<Products> getProducts() {
            return products;
        }

        public Categories(int id, @Nullable String name, @Nullable List<Products> products) {
            this.id = id;
            this.name = name;
            this.products = products;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(@Nullable String name) {
            this.name = name;
        }

        public void setProducts(@Nullable List<Products> products) {
            this.products = products;
        }
    }

    public class Rankings {
        @Nullable
        @SerializedName("ranking")
        @Expose
        public String rankingByTitle;

        @Nullable
        @SerializedName("date_added")
        @Expose
        public String dateAdded;

        @Nullable
        @SerializedName("products")
        @Expose
        public List<Products> products;

        public class Products {
            @PrimaryKey
            @NonNull
            @SerializedName("id")
            @Expose
            public int id;

            @Nullable
            @SerializedName("view_count")
            @Expose
            public String viewCount;

            public Products(int id, @Nullable String viewCount) {
                this.id = id;
                this.viewCount = viewCount;
            }

            public int getId() {
                return id;
            }

            public String getViewCount() {
                return viewCount;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setViewCount(@Nullable String viewCount) {
                this.viewCount = viewCount;
            }
        }

        public String getRankingByTitle() {
            return rankingByTitle;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public List<Products> getProducts() {
            return products;
        }

        public Rankings(@Nullable String rankingByTitle, @Nullable String dateAdded, @Nullable List<Products> products) {
            this.rankingByTitle = rankingByTitle;
            this.dateAdded = dateAdded;
            this.products = products;
        }

        public void setRankingByTitle(@Nullable String rankingByTitle) {
            this.rankingByTitle = rankingByTitle;
        }

        public void setDateAdded(@Nullable String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public void setProducts(@Nullable List<Products> products) {
            this.products = products;
        }
    }

    public Categories[] getCategories() {
        return categories;
    }

    public Rankings[] getRankings() {
        return rankings;
    }

    public DataModel(@Nullable Categories[] categories, @Nullable Rankings[] rankings) {
        this.categories = categories;
        this.rankings = rankings;
    }

    public void setCategories(@Nullable Categories[] categories) {
        this.categories = categories;
    }

    public void setRankings(@Nullable Rankings[] rankings) {
        this.rankings = rankings;
    }
}
