package com.sadashivsinha.heady.Model.TypeConverters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.sadashivsinha.heady.Model.DataModel;

public class CategoriesTypeConverter {
    @TypeConverter
    public static DataModel.Categories[] fromString(String categoriesJson) {
        if (categoriesJson == null) return null;

        Gson gson = new Gson();
        DataModel.Categories[] rankings = gson.fromJson(categoriesJson, DataModel.Categories[].class);
        return rankings;
    }

    @TypeConverter
    public static String toString(DataModel.Categories[] categoriesJson) {
        if (categoriesJson == null) return null;

        Gson gson = new Gson();
        String categoriesJsonFull = gson.toJson(categoriesJson);
        return categoriesJsonFull;
    }
}
