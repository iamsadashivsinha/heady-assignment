package com.sadashivsinha.heady.RecyclerViewAdapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.sadashivsinha.heady.CustomViews.CustomVariantView;
import com.sadashivsinha.heady.CustomViews.CustomVariantViewDetails;
import com.sadashivsinha.heady.Model.DataModel;
import com.sadashivsinha.heady.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Purpose – {Adapter for Main Tabs on the left}
 *
 * @author Sadashiv Sinha
 * Created on 2020-01-17
 */
public class MainTabItemsAdapter extends RecyclerView.Adapter<MainTabItemsAdapter.MyViewHolder> {

    private List<DataModel.Categories.Products> productsList;
    private int currentDataPosition;
    private Activity activity;

    int[] backgroundImages = {R.drawable.shop_one, R.drawable.shop_four, R.drawable.shop_three, R.drawable.shop_two, R.drawable.shop_five, R.drawable.shop_six};

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rootLayout;
        TextView tvTitleProduct, tvDateProduct;
        ImageView ivBackground;

        LinearLayout layoutVariants;
        RelativeLayout layoutClick;

        MyViewHolder(final View view) {
            super(view);

            rootLayout = view.findViewById(R.id.layout_data);
            tvTitleProduct = view.findViewById(R.id.tv_product_title);
            tvDateProduct = view.findViewById(R.id.tv_product_date);
            ivBackground = view.findViewById(R.id.iv_background);

            layoutClick = view.findViewById(R.id.layout_focus);
            layoutVariants = view.findViewById(R.id.layout_variants);
        }
    }

    public MainTabItemsAdapter(Activity activity, List<DataModel.Categories.Products> productsList, int currentDataPosition) {
        this.activity = activity;
        this.productsList = productsList;
        this.currentDataPosition = currentDataPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_main_tab_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final DataModel.Categories.Products productModel = productsList.get(position);
        holder.tvTitleProduct.setText(productModel.getName());
        holder.tvDateProduct.setText("Added on : " + formatDate(productModel.getDateAdded()));

        holder.layoutVariants.removeAllViews();
        for (int i = 0; i < productModel.getVariants().size(); i++) {
            holder.layoutVariants.addView(new CustomVariantView(holder.itemView.getContext(), returnProperColor(productModel.getVariants().get(i).getColor()), productModel.getVariants().get(i).getSize(), productModel.getVariants().get(i).getPrice()));

        }
        if (position == 1) {
            // right layout

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) holder.itemView.getContext().getResources().getDimension(R.dimen.height_item_card));
            params.setMargins(0, 150, 0, 0);
            holder.rootLayout.setLayoutParams(params);
        } else if (position % 2 != 0) {
            // right layout

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) holder.itemView.getContext().getResources().getDimension(R.dimen.height_item_card));
            params.setMargins(0, -30, 0, 0);
            holder.rootLayout.setLayoutParams(params);
        } else if (position != 0) {
            // left layout

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) holder.itemView.getContext().getResources().getDimension(R.dimen.height_item_card));
            params.setMargins(0, -170, 0, 0);
            holder.rootLayout.setLayoutParams(params);
        }

        int currentPosition = currentDataPosition + position;
        int imageArrayLength = backgroundImages.length - 1;

        if (imageArrayLength < currentPosition) currentPosition = position % imageArrayLength;

        holder.ivBackground.setImageResource(backgroundImages[currentPosition]);

        int finalCurrentPosition = currentPosition;
        holder.layoutClick.setOnClickListener(view -> openDialog(productModel, backgroundImages[finalCurrentPosition]));

    }

    public String formatDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date newDate = null;
        try {
            newDate = format.parse(date);
            format = new SimpleDateFormat("MMM dd,yyyy");

            return format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();

            return "";
        }
    }

    public void openDialog(DataModel.Categories.Products productModel, int image) {
        View mView = activity.getLayoutInflater().inflate(R.layout.bottom_sheet_product, null);
        BottomSheetDialog dialogSheet = new BottomSheetDialog(mView.getContext(), R.style.SheetDialog);
        dialogSheet.setContentView(mView);
        dialogSheet.show();

        LinearLayout layoutVariants = mView.findViewById(R.id.layout_variants);
        ImageView ivProduct = mView.findViewById(R.id.iv_product_image);

        CardView cardViewBuy = mView.findViewById(R.id.card_view_buy);
        TextView tvBuy = mView.findViewById(R.id.tv_buy);

        TextView tvProductTitle = mView.findViewById(R.id.tv_product_title);
        tvProductTitle.setText(productModel.getName());

        ivProduct.setImageResource(image);

        for (int i = 0; i < productModel.getVariants().size(); i++) {
            CustomVariantViewDetails viewVariant = new CustomVariantViewDetails(mView.getContext(), productModel.getVariants().get(i).getColor(), productModel.getVariants().get(i).getSize(), productModel.getVariants().get(i).getPrice());
            viewVariant.setOnClickListener(view -> {
                for (int i1 = 0; i1 < productModel.getVariants().size(); i1++) {
                    ((CustomVariantViewDetails) layoutVariants.getChildAt(i1)).changeSelectedStatus(false);
                }

                viewVariant.changeSelectedStatus(true);
                cardViewBuy.setCardBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
                tvBuy.setTextColor(activity.getResources().getColor(R.color.colorBlack));
                tvBuy.setText(activity.getResources().getString(R.string.text_buy_now));

                tvBuy.setOnClickListener(view1 -> {
                    Toast.makeText(mView.getContext(), mView.getContext().getString(R.string.item_purchase_success), Toast.LENGTH_SHORT).show();
                    dialogSheet.dismiss();
                });
            });
            layoutVariants.addView(viewVariant);
        }

    }

    public int returnProperColor(String colorName) {
        switch (colorName.toLowerCase()) {
            case "black":
                return R.color.colorBlack;

            case "brown":
                return R.color.colorBrown;

            case "red":
                return R.color.colorRed;

            case "blue":
                return R.color.colorBlue;

            case "green":
                return R.color.colorGreen;

            case "white":
                return R.color.colorWhite;

            default:
                return R.color.colorAccent;
        }
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }

}
