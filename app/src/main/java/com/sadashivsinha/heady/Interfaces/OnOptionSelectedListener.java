package com.sadashivsinha.heady.Interfaces;

public interface OnOptionSelectedListener {
    void onSelected(int option);
}
