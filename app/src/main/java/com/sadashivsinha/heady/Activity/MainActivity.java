package com.sadashivsinha.heady.Activity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.sadashivsinha.heady.CustomViews.CustomFilterView;
import com.sadashivsinha.heady.Interfaces.OnOptionSelectedListener;
import com.sadashivsinha.heady.Model.DataModel;
import com.sadashivsinha.heady.R;
import com.sadashivsinha.heady.RecyclerViewAdapters.MainTabItemsAdapter;
import com.sadashivsinha.heady.Utilities.DataViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

/**
 * Created by sadashivsinha on 16/01/20
 */

public class MainActivity extends AppCompatActivity implements OnOptionSelectedListener {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private DataViewModel viewModel;

    RecyclerView recyclerViewItems;
    MainTabItemsAdapter mainTabItemsAdapter;

    TabLayout tabLayoutMain;

    DataModel allData;

    LinearLayout layoutFilterOption;
    BottomSheetDialog dialogSheetFilter;
    TextView tvFilterOption;

    View loadingLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupViews();
        this.configureDagger();
        this.configureViewModel();
    }

    private void setupViews() {
        loadingLayout = findViewById(R.id.layout_loading);
        layoutFilterOption = findViewById(R.id.layout_filter);
        tvFilterOption = findViewById(R.id.tv_filter_option);
        recyclerViewItems = findViewById(R.id.recycler_view_items);

        recyclerViewItems.setLayoutManager(new GridLayoutManager(this, 2));

        tabLayoutMain = findViewById(R.id.tabs_main);
        tabLayoutMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (allData != null) {
                    mainTabItemsAdapter = new MainTabItemsAdapter(MainActivity.this, allData.getCategories()[tab.getPosition()].getProducts(), tab.getPosition());
                    recyclerViewItems.setAdapter(mainTabItemsAdapter);

                    recyclerViewItems.post(() -> recyclerViewItems.startLayoutAnimation());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        layoutFilterOption.setOnClickListener(view -> showFilterOptions(allData.getRankings()));
    }

    private void configureDagger() {
        AndroidInjection.inject(this);
    }

    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DataViewModel.class);
        viewModel.init();
        viewModel.getData().observe(this, this::updateUI);
    }

    private void updateUI(@Nullable DataModel data) {

        if (data != null) {
            loadingLayout.setVisibility(View.GONE);

            allData = data;

            tabLayoutMain.removeAllTabs();

            for (int i = 0; i < data.getCategories().length; i++)
                tabLayoutMain.addTab(tabLayoutMain.newTab().setText(data.getCategories()[i].getName()));

            tabLayoutMain.scrollTo(0, 0);

            mainTabItemsAdapter = new MainTabItemsAdapter(this, data.getCategories()[0].getProducts(), 0);
            recyclerViewItems.setAdapter(mainTabItemsAdapter);

            recyclerViewItems.post(() -> recyclerViewItems.startLayoutAnimation());

            changeTabsFont();
        }
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayoutMain.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    Typeface typeface;

                    if (Build.VERSION.SDK_INT >= 26) {
                        typeface = getResources().getFont(R.font.font_regular);

                    } else {
                        typeface = ResourcesCompat.getFont(this, R.font.font_regular);
                    }

                    ((TextView) tabViewChild).setTypeface(typeface);
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void showFilterOptions(DataModel.Rankings[] rankings) {
        View mView = getLayoutInflater().inflate(R.layout.bottom_sheet_filter_options, null);
        dialogSheetFilter = new BottomSheetDialog(mView.getContext(), R.style.SheetDialog);
        dialogSheetFilter.setContentView(mView);
        dialogSheetFilter.show();

        LinearLayout layoutFilterOptions = mView.findViewById(R.id.layout_filter_option);

        String rankingTitle;

        layoutFilterOptions.addView(new CustomFilterView(this, this, getResources().getString(R.string.title_all_collections), -1));

        for (int i = 0; i < rankings.length; i++) {

            rankingTitle = rankings[i].getRankingByTitle().toLowerCase();
            layoutFilterOptions.addView(new CustomFilterView(this, this, rankingTitle.substring(0, 1).toUpperCase() + rankingTitle.substring(1), i));

        }
    }

    @Override
    public void onSelected(int option) {
        if (dialogSheetFilter != null) dialogSheetFilter.dismiss();

        if (option == -1) {
            // Default option - All Collection
            tabLayoutMain.setVisibility(View.VISIBLE);
            viewModel.getData().observe(this, this::updateUI);
            tvFilterOption.setText(getResources().getString(R.string.title_all_collections));
        } else {
            tabLayoutMain.setVisibility(View.GONE);
            String rankingTitle = allData.getRankings()[option].getRankingByTitle();
            tvFilterOption.setText(rankingTitle.substring(0, 1).toUpperCase() + rankingTitle.substring(1));

            List<DataModel.Categories.Products> filteredDataProducts = new ArrayList<>();

            for (int i = 0; i < allData.getCategories().length; i++) {
                for (int j = 0; j < allData.getCategories()[i].getProducts().size(); j++) {
                    DataModel.Categories.Products currentProduct = allData.getCategories()[i].getProducts().get(j);

                    for (int k = 0; k < allData.getRankings()[option].getProducts().size(); k++) {
                        if (currentProduct.getId() == allData.getRankings()[option].getProducts().get(k).getId()) {
                            filteredDataProducts.add(currentProduct);
                        }

                    }
                }
            }

            mainTabItemsAdapter = new MainTabItemsAdapter(this, filteredDataProducts, 0);
            recyclerViewItems.setAdapter(mainTabItemsAdapter);
            recyclerViewItems.post(() -> recyclerViewItems.startLayoutAnimation());
        }
    }
}
